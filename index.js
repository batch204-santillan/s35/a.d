/*
	MINI ACTIITY
		- create an expressJS designated to port 4000 
		- create a new route w/ endpoint /hello and method GET
			- respond with "Hello WORLD!"
*/

	const express = require ("express");
	/*
		-mongoose is a package that allows creation of Schemas to model our data stuctures.
		- also has a number of methods for manipulating our database
	*/
	const mongoose = require ("mongoose");
	const app = express ();
	const port = 4000 ;

	
	/*
		MongoDB connection
			SYNTAX:
			mongoose.connect("mongodb+srv://rafsantillan:<PASSWORD>@batch204-santillan.1nyzknv.mongodb.net/<DATABASE NAME>?retryWrites=true&w=majority" ,
				{
					useNewUrlParser : true,
					useUnifiedTopology: true
				}
			);
	*/
	
	/*mongoose.connect ("mongodb+srv://rafsantillan:admin123@batch204-santillan.1nyzknv.mongodb.net/B204-to-dos?retryWrites=true&w=majority" ,
		{
			useNewUrlParser : true,
			useUnifiedTopology: true
		}
	); */

	/*
		Set notifications for connection success or failure
	*/
	let db = mongoose.connection;
	// if a connection error occured, output in the console
	db.on("error", console.error.bind(console, "Connection Error"));
	// if the conncetion is successfull, output in the console.
	db.once("open", ()=> console.log("We're connected to the cloud database."))

	/*
		Mongoose Schema
			- schemas determine the structure of the documents to written in our database.
			- this acts as our blueprints to our data

	*/

	const taskSchema = new mongoose.Schema ({
		name: String,
		status: {
			type: String,
			default: "pending"
		}
	});

	/*
		Models
			- the 1st parameter of the mongoose model will indicate the collection in where to store the data
			- the 2nd parameter is used to specify the schema/blueprint of the documents that will be stored in our MongoDB collection
	*/
	const Task = mongoose.model("Task" , taskSchema);



	app.use(express.json());

	app.get ("/hello", (req, res) => {
		res.send ("Hello world!")
	});


/*
	ROUTE to create a task
*/
	app.post ("/tasks" , (req, res) => 
	{
		// prints the current database in our terminal
		//console.log(req.body);
		Task.findOne({name: req.body.name}, (err, result) =>
		{	if (result !== null && result == req.body.name) 
				{	return res.send("Duplicate Task Found")
				} 
			else 
				{	let newTask = new Task (
						{
							name: req.body.name
						});
					newTask.save ((saveErr,savedTask)=> 
						{
							if (saveErr) 
							{	return console.error (saveErr)
							}
							else 
							{	return res.status(201).send("New Task created");
							}
						});
				}
		});

	});

/*
	ROUTE to GET ALL TASKS
*/
	app.get("/tasks", (req, res) => 
	{	Task.find({}, (err, result) => 
		{	if (err) 
			{	return console.log(err);
			}
			else
			{	return res.status(200).json (
				{
					data: result
				})
			}
		});
	});


/*
	ACTIVITY!!!
		1. Create a User schema.
		2. Create a User model.
		3. Create a POST route that will access the "/signup" route that will create a user.
		4. Process a POST request at the "/signup" route using postman to register a user.
		5. Create a git repository named S35.
		6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
		7. Add the link in Boodle.
*/
mongoose.connect ("mongodb+srv://rafsantillan:admin123@batch204-santillan.1nyzknv.mongodb.net/Users?retryWrites=true&w=majority" ,
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
); 
	
	// 1. new user schema
	const userSchema = new mongoose.Schema ({
		username: String,
		password: String
	});

	// 2. user model
	const User = mongoose.model ("User", userSchema);

	// 3. create a POST route
	app.post ("/signup", (req, res) =>
	{
		User.findOne({name: req.body.username}, (err, result) =>
		{ 	if (res !== null && res == req.body.username)
				{	return res.send ("User already exists!")
				}
			else
				{	let newUser = new User (
					{
						username: req.body.username,
						password: req.body.password
					});
					newUser.save ((saveErr,savedUser)=> 
						{
							if (saveErr) 
							{	return console.error (saveErr)
							}
							else 
							{	return res.status(201).send("New User created");
							}
						}); 
				}


		});
	});









// ------- ALWAYS AT THE BOTTOM -------- //
	app.listen(port,() => console.log(`Server running at port: ${port}.`))